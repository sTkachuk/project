<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('404', 'ErrorHandlerController@errorCode404')->name('404');
//Route::get('405', 'ErrorHandlerController@errorCode405')->name('405');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/publication', 'HomeController@index')->name('publication.index');

Route::get('/post/create', 'PostController@create')->name('post.create');
Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
Route::get('/delete_file/{id}/{file}', 'PostController@delete_file')->name('post.delete_file');
Route::get('/post/show', 'PostController@show')->name('post.show');
Route::get('/post/delete/{id}', 'PostController@delete')->name('post.delete');
Route::get('/download_file/{id}/{file}', 'PostController@download_file')->name('post.download_file');
Route::post('/post/store', 'PostController@store')->name('post.store');
Route::post('/post/update/{id}', 'PostController@update')->name('post.update');

Route::get('/category/show', 'CategoryController@show')->name('category.show');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
Route::get('/category/delete/{id}', 'CategoryController@delete')->name('category.delete');
Route::post('/category/store', 'CategoryController@store')->name('category.store');
Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');

Route::get('/show/sort/{num}', 'IndexController@show')->name('index.show');
Route::get('/show/{id}', 'IndexController@show_post')->name('index.show_post');
Route::post('/sort', 'IndexController@sort')->name('index.sort');

Route::get('/user_show/@{nickname}', 'UserController@show')->name('user.show');
Route::get('/@{nickname}/edit', 'UserController@edit')->name('user.edit')->middleware('auth');
Route::get('/@{nickname}/delete', 'UserController@delete')->name('user.delete')->middleware('auth');
Route::post('/@{nickname}/update', 'UserController@update')->name('about_user.update')->middleware('auth');

Route::get('/comment/delete/{id}', 'CommentController@delete')->name('comment.delete');
Route::post('/comment/add/{id}', 'CommentController@add')->name('comment.add');

