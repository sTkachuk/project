<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Fillable fields for a user.
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the posts associated with the given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
     * Get the information about user associated with the given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function about_user()
    {
        return $this->hasOne('App\AboutUser');
    }

    /**
     * Get the comments associated with the given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments_by_user()
    {
        return $this->hasMany('App\Comment');
    }
}
