<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilesInPost extends Model
{
    /**
     * Table name is "files"
     *
     * @var string
     */
    protected $table = "files";

    /**
     * Fillable fields for a files.
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'path', 'filename',
    ];

    /**
     * Get the post associated with the given file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
