<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUser extends Model
{
    /**
     * @var string
     */
    protected $table = 'about_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'nickname', 'avatar_path', 'about_user'
    ];

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
