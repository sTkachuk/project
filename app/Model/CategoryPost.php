<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model
{
    /**
     * @var string
     */
    protected $table = 'category_post';
}
