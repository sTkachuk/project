<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Fillable fields for a comment.
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'post_id', 'user_id', ];

    /**
     * Get the user associated with the given comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_comments()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the post associated with the given comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post_comments()
    {
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }
}
