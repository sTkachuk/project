<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * Fillable fields for a post.
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];

    /**
     * Get the user associated with the given posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get the files associated with the given post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filesInPost()
    {
        return $this->hasMany('App\FilesInPost');
    }

    /**
     * Get the categories associated with the given post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category')->withTimestamps()->withPivot('post_id');
    }

    /**
     * Get the comments associated with the given post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments_to_post()
    {
        return $this->hasMany('App\Comment');
    }

}
