<?php

if (! function_exists('fa_class')) {
    /**
     * Функція для визначення класу font_awesome
     * відносно заданого розширення файлу
     *
     * @param $extension
     * @return string
     */
    function fa_class($extension){
        switch($extension)
        {
            case '.doc':
            case '.docx':
                return 'fa-file-word-o';
            case '.xls':
            case '.xlsx':
                return 'fa-file-excel-o';
            case '.txt':
                return 'fa-file-text';
            case '.ppt':
            case '.pptx':
                return 'fa-file-powerpoint-o';
            case '.mp3':
            case '.flac':
            case '.ape':
            case '.ogg':
            case '.waw':
            case '.ac3':
            case '.wma':
            case '.m4a':
            case '.aac':
                return 'fa-file-audio-o';
            case '.bmp':
            case '.jpg':
            case '.jpeg':
            case '.png':
            case '.gif':
            case '.tiff':
            case '.ico':
            case '.raw':
                return 'fa-file-image-o';
            case '.avi':
            case '.wmw':
            case '.mkv':
            case '.3gp':
            case '.flv':
            case '.mpeg':
            case '.mp4':
            case '.mov':
            case '.vob':
                return 'fa-file-video-o';
            case '.rar':
            case '.zip':
            case '.7z':
            case '.tar':
            case '.gzip':
            case '.gz':
            case '.jar':
                return 'fa-file-archive-o';
            case '.html':
            case '.htm':
            case '.php':
            case '.css':
            case '.js':
                return 'fa-file-code-o';
            case '.pdf':
                return 'fa-file-pdf-o';
            default:
                return 'fa-file';
        }
    }
}