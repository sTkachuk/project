<?php

namespace App\Policies;

use App\AboutUser;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AboutUserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given about_user can be edited by the user.
     *
     * @param User $user
     * @param AboutUser $aboutUser
     * @return bool
     */
    public function edit(User $user, AboutUser $aboutUser)
    {
        return $user->id === $aboutUser->user_id;
    }

    /**
     * Determine if the given about_user can be deleted by the user.
     *
     * @param User $user
     * @param AboutUser $aboutUser
     * @return bool
     */
    public function delete(User $user, AboutUser $aboutUser)
    {
        return $user->id === $aboutUser->user_id;
    }
}
