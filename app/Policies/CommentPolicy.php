<?php

namespace App\Policies;

use App\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given comment can be deleted by the user.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function delete_comment(User $user, Comment $comment)
    {
        return $user->id === $comment->user_id;
    }
}
