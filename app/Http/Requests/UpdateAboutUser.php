<?php

namespace App\Http\Requests;

use App\AboutUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateAboutUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $model = AboutUser::where('nickname', $this->route('nickname'))->first();
        if ($model && Auth::user()->can('edit', $model)){
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'file|mimes:jpeg,bmp,png,jpg',
            'about_user' => 'required|string|max:500'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'about_user.max' => 'The about yourself may not be greater than 500 characters.',
        ];
    }
}
