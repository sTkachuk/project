<?php

namespace App\Http\Requests;

use App\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = Post::find($this->route('id'));
        if ($post && $this->user()->can('update', $post)){
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        $post = Auth::user()->posts()->where('id', $id)->first();
        $count = 5 - $post->filesInPost->count();
        return [
            'title' => ['required', 'string', 'max:50', Rule::unique('posts')
                ->where('user_id', Auth::id())
                ->whereNot('id', [$post->id])],
            'description' => 'required|string|max:200',
            'files' => ['array', 'max:'.$count,
                function($attribute, array $value, $fail) use ($post) {
                    $arr_db = array();
                    foreach ($post->filesInPost as $file_db){
                        array_push($arr_db, $file_db->filename);
                    }
                    $arr_input = array();
                    foreach ($value as $file_input){
                        array_push($arr_input, $file_input->getClientOriginalName());
                    }

                    if (!empty(array_intersect($arr_input, $arr_db))) {
                        return $fail('The '.$attribute.' already has files with such names: '
                            .implode(', ', array_intersect($arr_input, $arr_db)));
                    }

                    return true;
                }],
            'content' => 'required'
        ];
    }
}
