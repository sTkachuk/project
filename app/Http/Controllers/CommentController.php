<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\AddComment;
use App\Post;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * CommentController constructor.
     */
    function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Функція додавання коментаря з попередньою валідацією в form request
     *
     * @param AddComment $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add(AddComment $request, $id)
    {
        $comment = new Comment();
        $comment->content = $request->get('content');
        $comment->user_id = Auth::id();
        $comment->post_id = $id;
        $comment->save();

        return redirect(route('index.show_post', ['id' => $id]));
    }

    /**
     * Функція видаження коментаря по ідентифікатору
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment && Auth::user()->can('delete_comment', $comment)){
            $comment->delete();

            return redirect()->back();
        }

        return redirect()->back()->with('massage', 'You can not delete this comment');
    }
}
