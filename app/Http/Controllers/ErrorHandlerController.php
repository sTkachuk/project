<?php

namespace App\Http\Controllers;

class ErrorHandlerController extends Controller
{
    /**
     * Функція для відображення view з 404 помилкою
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function errorCode404()
    {
        return view('errors.404');
    }

    /**
     * Функція для відображення view з 405 помилкою
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function errorCode405()
    {
        return view('errors.405');
    }
}
