<?php

namespace App\Http\Controllers;

use App\Category;
use App\FilesInPost;
use App\Http\Requests\UpdateBlogPost;
use App\Post;
use App\Http\Requests\StoreBlogPost;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Функція відображення форми для додавання поста
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all();

        return view('post.create', compact('categories'));
    }

    /**
     * Функція внесення даних з форми до бази даних
     * Валідація в form request StoreBlogPost
     *
     * @param StoreBlogPost $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreBlogPost $request)
    {
        $post = new Post();

        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->description = $request->get('description');
        $post->user_id = Auth::id();
        $post->save();

        self::createFilesInPost($request, $post); // додавання файлів до поста

        $post->categories()->sync($request->input('category'));//синхронізація даних в проміжній таблиці

        return redirect(route('post.show'));
    }

    /**
     * Функція відображення постів певного користувача
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(){

        $posts = Post::where('user_id', Auth::id())->paginate(5);

        return view('post.show', compact('posts'));
    }

    /**
     * Функція видалення поста по ідентифікатору
     * Видалення дерикторії поста
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        if (Post::find($id) && Auth::user()->can('delete', Post::find($id))){
            $post = self::getPostById($id);
            $post->categories()->detach();
            $post->filesInPost()->delete();
            Storage::disk('public')->deleteDirectory('users/user'.Auth::id().'/posts/'.$post->id);

            $post->delete();

            return redirect(route('post.show'))
                ->with('status', "Post with id {$id} was successfully deleted");
        }

        return redirect()->back()->with('massage', 'You do not have the right to change url.');
    }

    /**
     * Функція для завантаження файлів з браузера
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download_file($id, $file_name)
    {
        if (Post::find($id) && Post::find($id)->filesInPost()->where('filename', $file_name)->first()){
            $post = Post::find($id);
            $path = $post->filesInPost()->where('filename', $file_name)->first()->path;

            return response()->download('storage/'.$path);
        }

        return redirect()->back();

    }

    /**
     * Функція для видалення усіх або окремого файлу з поста
     *
     * @param $id
     * @param $file_name
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete_file($id, $file_name)
    {
        if (self::getPostById($id)){
            $post = self::getPostById($id);

            if ($post->filesInPost()->where('filename', $file_name)->first()){
                $path = $post->filesInPost()->where('filename', $file_name)->first()->path;
                Storage::delete('public/'.$path);
                $post->filesInPost()->where('filename', $file_name)->delete();

                return redirect(route('post.edit', ['id' => $id]));
            }elseif ($file_name == 'all'){
                $post->filesInPost()->delete();
                Storage::disk('public')->deleteDirectory('users/user'.Auth::id().'/posts/'.$post->id);

                return redirect(route('post.edit', ['id' => $id]));
            }

            return redirect()->back();
        }

        return redirect()->back();
    }

    /**
     * функція відобаження форми для редагування даних поста
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (Post::find($id) && Auth::user()->can('update', Post::find($id))){
            $post = self::getPostById($id);
            $files = $post->filesInPost;
            $categories= Category::all();
            $arr = array();
            foreach ($post->categories as $category){
                array_push($arr, $category->id);
            }

            return view('post.edit', compact('post', 'categories', 'arr', 'files'));
        }

        return redirect()->back()->with('massage', 'You do not have the right to change url.');
    }

    /**
     * Фунція оновлення даних з форми редагування поста
     * Валідація форми в form request UpdateBlogPost
     *
     * @param $id
     * @param UpdateBlogPost $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateBlogPost $request)
    {
        $post = self::getPostById($id);// отримання об'єкта Post по ідентифікатору
        self::createFilesInPost($request, $post);

        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->description = $request->get('description');
        $post->user_id = Auth::id();

        $post->save();

        $post->categories()->sync($request->input('category')); // синхронізація даних в проміжній таблиці

        return redirect(route('index.show_post', compact('post')));
    }

    /**
     * Отримання об'єкта Post по ідентифікатору
     *
     * @param $id
     * @return mixed
     */
    private static function getPostById($id)
    {
        return Auth::user()->posts()->where('id', $id)->first();
    }

    /**
     * Функція внесення файлів до бази даних
     *
     * @param $request
     * @param Post $post
     */
    private static function createFilesInPost($request, Post $post)
    {
        if (!empty($request->file())) {
            foreach ($request->file('files') as $file) {
                $file->storeAs('users/user' . Auth::id() . '/posts/' . $post->id, $file->getClientOriginalName(), 'public');
                $path = $file->storeAs('users/user' . Auth::id() . '/posts/' . $post->id, $file->getClientOriginalName(), 'public');

                FilesInPost::create([
                    'post_id' => $post->id,
                    'path' => $path,
                    'filename' => $file->getClientOriginalName()
                ]);
            }
        }
    }
}