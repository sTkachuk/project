<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class IndexController extends Controller
{
    /**
     * Функція для відображення посортованих постів
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($num, Request $request)
    {
        if ($num == 1){
            $posts = Post::orderBy('updated_at', 'desc')->paginate(5);

            return view('index.show', compact('posts'));
        }elseif ($num == 2){
            $arr = array();

            foreach (Category::orderBy('name')->get() as $category){
                $array = $this->help_function($category->posts_category);
                $arr = array_merge($arr, $array);
            }
            $posts = array();
            foreach (array_unique($arr) as $item) {
                array_push($posts, Post::find($item));
            }

            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            // Create a new Laravel collection from the array data
            $itemCollection = collect($posts);

            // Define how many items we want to be visible in each page
            $perPage = 5;

            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

            // set url path for generted links
            $paginatedItems->setPath($request->url());

            return view('index.show', ['posts' => $paginatedItems]);
        }elseif ($num == 3){
            $posts = Post::orderBy('user_id', 'desc')->paginate(5);

            return view('index.show', compact('posts'));
        }else{
            $posts = Post::orderBy('view_count', 'desc')->paginate(5);

            return view('index.show', compact('posts'));
        }
    }

    /**
     * Функція для відображеня певного поста
     * Event для відслідковування кількості переглядів поста
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_post($id){
        $post = Post::find($id);
        if (is_null($post)){
            return redirect()->back();
        }
        $categories = $post->categories->sortBy('name');
        $arr = array();
        foreach ($categories as $category){
            array_push($arr, $category->name);
        }
        $categories = implode(', ', $arr);
        $files = $post->filesInPost;
        $comments = Comment::where('post_id', $post->id)->paginate(10);
        event('postHasViewed', $post);

        return view('index.show_post', compact('post', 'categories', 'files', 'comments', 'user'));
    }

    /**
     * Функція визначення методу сортування
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sort(Request $request)
    {
        $num = $request->get('sort');

        return redirect(route('index.show', compact( 'num')));
    }

    /**
     * @param $array
     * @return array
     */
    protected function help_function($array){
        $arr = array();
        foreach ($array as $item){
            array_push($arr, $item->id);
        }

        return $arr;
    }
}
