<?php

namespace App\Http\Controllers;

use App\AboutUser;
use App\Http\Requests\UpdateAboutUser;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Функція відображення інформації про користувача
     *
     * @param $nickname
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($nickname)
    {
        $model = AboutUser::where('nickname', $nickname)->firstOrFail();
        $user = $model->user;
        $posts = Post::where('user_id', $user->id)->paginate(5);

        return view('user.show', compact('user', 'model', 'posts'));
    }

    /**
     * Функція відображення view для редагування інформації про користувача
     *
     * @param $nickname
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($nickname)
    {
        $model = AboutUser::where('nickname', $nickname)->firstOrFail();
        if (Auth::user()->can('edit', $model)){
            return view('user.edit', compact('model'));
        }

        return redirect()->back();
    }

    /**
     * Функція оновлення даних про користувача
     *
     * @param $nickname
     * @param UpdateAboutUser $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($nickname, UpdateAboutUser $request)
    {
        $model = AboutUser::where('nickname', $nickname)->firstOrFail();
        $model->about_user = $request->get('about_user');
        if (!$request->file('avatar')){
            $model->avatar_path = '';
        }else {
            $model->avatar_path = $request->file('avatar')
                ->storeAs(
                    'users/user'.Auth::id(),
                    'avatar.'.$request->file('avatar')->extension(),
                    'public'
                );
        }
        $model->save();

        return redirect(route('user.show', compact('nickname')));
    }

    /**
     * Функція видалення даних про користувача
     *
     * @param $nickname
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($nickname)
    {
        $model = AboutUser::where('nickname', $nickname)->firstOrFail();
        if (Auth::user()->can('delete', $model)) {
            Storage::delete('public/' . $model->avatar_path);
            $model->about_user = '';
            $model->avatar_path = '';
            $model->save();

            return redirect(route('user.show', compact('nickname')));
        }

        return redirect()->back();
    }
}
