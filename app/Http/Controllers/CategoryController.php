<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Функція для відображення view з формою для додавання категорій
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Функція додавання даних з форми в базу
     * Валідація в form request StoreCategory
     *
     * @param StoreCategory $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreCategory $request)
    {
        Category::create([
            'name' => $request->get('category')
        ]);

        return redirect(route('category.show'));
    }

    /**
     * Функція відображення цсіх категорій у view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $categories = Category::all();
        $num = 1;

        return view('categories.show', compact('categories', 'num'));
    }

    /**
     * Функція відображення форми для редагування даних про категорію
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (Category::find($id)){
            $category = Category::find($id);

            return view('categories.edit', compact('category'));
        }

        return redirect()->back()->with('massage', 'You do not have the right to change url.');
    }

    /**
     * Функція оновлення даних про категорію
     * Валідація в form request UpdateCategory
     *
     * @param $id
     * @param UpdateCategory $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateCategory $request)
    {
        $category = Category::find($id);
        $category->name = $request->get('category');
        $category->save();

        return redirect(route('category.show'));
    }

    /**
     * Функція видалення категорії
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        if (Category::find($id)){
            $category = Category::find($id);
            $category->posts_category()->detach();
            $category->delete();

            return redirect(route('category.show'));
        }

        return redirect()->back();
    }
}
