@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <form action="{{ route('index.sort') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <select name="sort" id="sort" class="custom-select">
                        <option value="1">Last news</option>
                        <option value="2">Category</option>
                        <option value="3">Author</option>
                        <option value="4">Popularity</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Sort</button>
            </form>
        </div>
        @if($posts->count() != 0)
            @foreach ($posts as $post)
                <div class="jumbotron">
                    <h2>{{$post->title}}</h2>
                    <h3>{{$post->description}}</h3>
                    <p>
                        <a href="{{route('user.show', ['id' => $post->posts_user->about_user->nickname])}}" role="button">{{ $post->posts_user->name }}</a>
                    </p>
                    <p>
                        Quantity of views: {{ $post->view_count }}
                    </p>
                    <p>
                        <a class="btn btn-lg btn-success" href="{{route('index.show_post', ['id' => $post->id])}}" role="button">Review</a>
                    </p>
                </div>
            @endforeach
        @endif
        <div class="center" style="display: flex; justify-content: center;">
            {{ $posts->links() }}

        </div>
    </div>
@endsection