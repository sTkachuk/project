@extends('layouts.app')

@section('content')
    <div class="container">
        <div style="margin: 20px 0;">
            <a class="btn btn-lg btn-success" href="{{ route('post.create') }}">New post</a>
        </div>
        <div class="jumbotron">
            <h2><strong>Title:</strong> {{$post->title}}</h2>
            <h4><strong>Brief description: </strong>{{$post->description}}</h4>
            <h4><strong>Content: </strong></h4>
            {!! $post->content !!}
            <br>
            <br>
            <h5>
                Category: {{ $categories }}
            </h5>
            <div>
                <p>
                    @foreach($files as $file)
                        <a href="{{ route('post.download_file', ['id' => $post->id, 'file' => $file->filename]) }}">
                            <i class="fa {{ fa_class('.'.pathinfo($file->path)['extension']) }} fa-3x" aria-hidden="true" title="{{ $file->filename }}"></i>
                        </a>
                    @endforeach
                </p>
            </div>
            <h3>Quantity of view: {{ $post->view_count }}</h3>
            <h3>Comments: </h3>
            @if (session('status-massage'))
                <div class="alert alert-danger">
                    {{ session('status-massage') }}
                </div>
            @endif
            <form action="{{ route('comment.add', ['id' => $post->id]) }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="cont">Enter text</label>
                    <textarea class="form-control" id="cont" name="content"></textarea>
                </div>
                <button type="submit" class="btn btn-success btn-sm">Add comment</button>
            </form>
            <div class="comments">
                @foreach($comments as $comment)
                    <div class="comment-item">
                        <div class="user-item">
                            @if(\App\AboutUser::where('user_id', $comment->user_id)->first()->avatar_path)
                                <img src="{{ asset('storage/'.\App\AboutUser::where('user_id', $comment->user_id)->first()->avatar_path) }}" alt="user" width="80px" height="80px" style="border-radius: 40px">
                            @else
                                <img src="{{ asset('images/user.jpg') }}" alt="user" width="80px" height="80px" style="border-radius: 40px">
                            @endif
                            <div>
                                <a href="{{route('user.show', ['nickname' => $comment->user_comments->about_user->nickname])}}" role="button">
                                    {{ $comment->user_comments->name }}
                                </a>
                            </div>

                        </div>
                        <div class="content-item">{{ $comment->content }}</div>
                        <div class="close-item">
                            @can('delete_comment', $comment)
                                <a href="{{ route('comment.delete', ['id' => $comment->id]) }}">
                                    <img src="{{ asset('images/close.png') }}" alt="close" width="25px" height="25px">
                                </a>
                            @endcan
                        </div>
                    </div>
                @endforeach
                <div class="center" style="display: flex; justify-content: center;">
                    {{ $comments->links() }}
                </div>
            </div>
            <div>
                @auth
                    @can('update', $post)
                        <a class="btn btn-lg btn-success" href="{{route('post.edit', ['id' => $post->id])}}" role="button" style="margin: 10px;">Edit</a>
                        <a class="btn btn-lg btn-success" href="{{route('post.delete', ['id' => $post->id])}}" role="button" style="margin: 10px;">Delete</a>
                    @endcan
                    <a class="btn btn-lg btn-success" href="{{route('index.show', ['num' => 1])}}" role="button" style="margin: 10px;">Back to posts</a>
                @else
                    <a class="btn btn-lg btn-success" href="{{route('index.show', ['num' => 1])}}" role="button" style="margin: 10px;">Back to posts</a>
                @endauth
            </div>
        </div>
    </div>
@endsection
