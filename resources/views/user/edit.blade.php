@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif
    <div class="container-fluid">
        <form action="{{ route('about_user.update', ['nickname' => $model->nickname]) }}" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="avatar">Avatar: </label>
                    <input type="file" class="form-control-file" id="avatar" name="avatar">
                </div>

                <div class="form-group">
                    <label for="about_user">Enter a brief information about yourself: </label>
                    <textarea class="form-control" name="about_user" rows="5" required>{{ $model->about_user }}</textarea>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-success">Save</button>
                <a class="btn btn-md btn-success" href="{{ route('user.show', ['nickname' => $model->nickname] )}}" role="button" style="margin: 10px;">Back</a>
            </div>
        </form>
    </div>
@endsection