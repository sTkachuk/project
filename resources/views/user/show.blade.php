@extends('layouts.app')

@section('content')
    <div class="container">
        <h4>User name: {{ $user->name }}</h4>
        @if($model->about_user)
            <div class="show_user">
                <div class="about_user">
                    <p>{{ $model->about_user }}</p>
                </div>
                <div class="avatar">
                    @if($model->avatar_path)
                        <img class="img_avatar" src="{{ asset('storage/'.$model->avatar_path) }}" alt="{{ $user->name }}" width="120px" height="120px">
                    @else
                        <img class="img_avatar" src="{{ asset('images/user.jpg') }}" alt="{{ $user->name }}" width="120px" height="120px">
                    @endif
                </div>
            </div>
            @auth
                @can('edit', $model)
                    <div style="margin: 20px 0;">
                        <a class="btn btn-lg btn-success" href="{{route('user.edit', ['nickname' => $model->nickname])}}" role="button">Edit</a>
                        <a class="btn btn-lg btn-success" href="{{route('user.delete', ['nickname' => $model->nickname])}}" role="button">Delete</a>
                    </div>
                @endcan
            @endauth
            @if ($posts->count() != 0)
                <h3>Posts by {{ $user->name }}:</h3>
                @foreach ($posts as $post)
                    <div class="jumbotron">
                        <h2>{{$post->title}}</h2>
                        <h3>{{$post->description}}</h3>
                        <p><a class="btn btn-lg btn-success" href="{{route('index.show_post', ['id' => $post->id])}}" role="button">Review</a></p>
                    </div>
                @endforeach
                <div class="center" style="display: flex; justify-content: center;">
                    {{ $posts->links() }}
                </div>
            @endif
        @else
            @if($posts->count() != 0)
                @can('edit', $model)
                    <p><a class="btn btn-lg btn-success" href="{{route('user.edit', ['nickname' => $model->nickname])}}" role="button">Add information about yourself</a></p>
                @else
                    <h3>This user has not specify additional information about yourself, but you can view his posts.</h3>
                @endcan
                <h3>Posts by {{ $user->name }}:</h3>
                @foreach ($posts as $post)
                    <div class="jumbotron">
                        <h2>{{$post->title}}</h2>
                        <h3>{{$post->description}}</h3>
                        <p><a class="btn btn-lg btn-success" href="{{route('index.show_post', ['id' => $post->id])}}" role="button">Review</a></p>
                    </div>
                @endforeach
                <div class="center" style="display: flex; justify-content: center;">
                    {{ $posts->links() }}
                </div>
            @else
                @can('edit', $model)
                    <p><a class="btn btn-lg btn-success" href="{{route('user.edit', ['nickname' => $model->nickname])}}" role="button">Add info about yourself</a></p>
                @else
                    <h2>This user has not specify additional information about yourself</h2>
                @endcan
            @endif
        @endif
    </div>
@endsection
