@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif
    <div class="container-fluid">
        <form action="{{ route('post.update', ['id' => $post->id]) }}" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                {{ csrf_field() }}
                <div class="form-group">
                    <p>
                        <select title="category-list" multiple name="category[]" class="custom-select">
                            @foreach($categories as $key => $category)
                                @if( in_array($category->id, $arr) )
                                    <option value="{{$category->id}}" selected>{{ $category->name }}</option>
                                @else
                                    <option value="{{$category->id}}">{{ $category->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </p>
                </div>

                <div>
                    @if($files->count() == 0)
                        <p>You do not have any files attached to this post yet. Add now?</p>
                        <div class="form-group">
                            <label for="files">Add file: </label>
                            <input type="file" class="form-control-file" id="files" multiple  name="files[]">
                        </div>
                    @else
                        These files have already been added:
                        <div class="wrap">
                            @foreach($files as $file)
                                <div class="wrap_item">
                                    <a class="url1" href="{{ route('post.download_file', ['id' => $post->id, 'file' => $file->filename]) }}">
                                        <i class="fa {{ fa_class('.'.pathinfo($file->path)['extension']) }} fa-3x" aria-hidden="true" title="{{ $file->filename }}"></i>
                                    </a>
                                    <a class="url2" href="{{ route('post.delete_file', ['id' => $post->id, 'file' => $file->filename]) }}">Delete</a>
                                </div>
                            @endforeach
                            @unless($files->count() == 1)
                                <a href="{{ route('post.delete_file', ['id' => $post->id, 'file' => 'all']) }}">Delete all</a>
                            @endunless
                        </div>
                        To choose other files?
                        <div class="form-group">
                            <label for="files">Other file: </label>
                            <input type="file" class="form-control-file" id="files" multiple  name="files[]">
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" id="title" class="form-control" name="title" value="{{$post->title}}">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" id="description" class="form-control" name="description" value="{{$post->description}}">
                </div>

                <div class="form-group">
                    <label for="content">Enter text</label>
                    <textarea class="form-control" id="content" name="content">{{$post->content}}</textarea>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-success">Save</button>
                <a class="btn btn-md btn-success" href="{{ route('index.show_post', ['id' => $post->id] )}}" role="button" style="margin: 10px;">Back</a>
            </div>
        </form>
    </div>
@endsection