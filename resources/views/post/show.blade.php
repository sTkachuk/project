@extends('layouts.app')

@section('content')
    <div class="container">
        @if($posts->count() == 0)
            <div class="alert-success alert">
                <p>You do not have any posts yet. Create a new post?</p>
            </div>
        @endif
        <div style="margin: 20px 0;">
            <a class="btn btn-lg btn-success" href="{{ route('post.create') }}">New post</a>
            <a class="btn btn-lg btn-success" href="{{ route('index.show', ['num' => 1]) }}">Back to all posts</a>
        </div>
        @if($posts->count() != 0)
            @foreach ($posts as $post)
                <div class="jumbotron">
                    <h2>{{$post->title}}</h2>
                    <h3>{{$post->description}}</h3>
                    <p><a class="btn btn-lg btn-success" href="{{route('index.show_post', ['id' => $post->id])}}" role="button">Review</a></p>
                </div>
            @endforeach
        @endif
        <div class="center" style="display: flex; justify-content: center;">
            {{ $posts->links() }}
        </div>
    </div>
@endsection
