@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif
    <div class="container-fluid">
        <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                {{ csrf_field() }}
                <div class="form-group">
                    <p>
                        <select multiple name="category[]" class="custom-select">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </p>
                </div>
                
                <div class="form-group">
                    <label for="files">Other files</label>
                    <input type="file" class="form-control-file" id="files" multiple  name="files[]">
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" id="title" class="form-control" name="title">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" id="description" class="form-control" name="description">
                </div>

                <div class="form-group">
                    <label for="content">Enter text</label>
                    <textarea class="form-control descript" id="content" name="content"></textarea>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection