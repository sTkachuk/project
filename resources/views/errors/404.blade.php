<!DOCTYPE html>
<html>
<head>
    <title>404 Page Not Found Error</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div>
        <div class="alert-danger alert">
            You do not have the right to change url.
        </div>
    </div>
    <h1>Sorry, Page Not Found 404</h1>
</body>
</html>