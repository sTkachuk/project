@extends('layouts.app')

@section('content')
    <div class="container">
        <div style="margin: 20px 0;">
            <a class="btn btn-md btn-success" href="{{ route('category.create') }}">New category</a>
        </div>
        @if($categories->isNotEmpty())
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name category</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <th scope="row">{{ $num++ }}</th>
                        <td>{{ $category->name }}</td>
                        <td><a class="btn btn-sm btn-success" href="{{ route('category.edit', ['id' => $category->id]) }}">Edit</a></td>
                        <td><a class="btn btn-sm btn-outline-warning" href="{{ route('category.delete', ['id' => $category->id]) }}">Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
@endsection
