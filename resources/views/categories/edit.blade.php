@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif
        <form action="{{ route('category.update', ['id' => $category->id]) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="category">Name category</label>
                <input type="text" name="category" id="category" value="{{ $category->name }}">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection